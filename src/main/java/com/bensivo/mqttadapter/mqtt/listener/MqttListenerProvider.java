package com.bensivo.mqttadapter.mqtt.listener;

import org.springframework.stereotype.Component;
import java.util.HashMap;

/**
 * Manages listener instances, for reuse.
 */
@Component
public class MqttListenerProvider {
    private HashMap<String, MqttListener> listenerMap = new HashMap<>();

    /**
     * Set the registered listener for a topic
     */
    public void setListener(String topic, MqttListener listener) {
        this.listenerMap.put(topic, listener);
    }

    /**
     * Return the registered listener for a topic
     */
    public MqttListener getListener(String topic) {
        return this.listenerMap.get(topic);
    }
}
