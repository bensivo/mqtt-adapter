package com.bensivo.mqttadapter.mqtt.listener;

import com.bensivo.mqttadapter.mqtt.MqttMessage;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

/**
 * Receives messages from the mqtt client, and exposes the stream as a Reactor Flux
 */
public class MqttListener {
    private Sinks.Many<MqttMessage> sink = Sinks.many().multicast().directAllOrNothing();

    public Flux<MqttMessage> getFlux() {
        return this.sink.asFlux();
    }

    public void onMessageReceived(MqttMessage message) {
        sink.tryEmitNext(message);
    }
}
