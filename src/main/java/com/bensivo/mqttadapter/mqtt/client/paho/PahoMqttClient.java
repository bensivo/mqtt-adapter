package com.bensivo.mqttadapter.mqtt.client.paho;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import com.bensivo.mqttadapter.mqtt.listener.MqttListener;

@Component
public class PahoMqttClient implements com.bensivo.mqttadapter.mqtt.client.MqttClient {

    Logger logger = LoggerFactory.getLogger(getClass());

    MqttClient client = null;

    HashMap<String, ArrayList<MqttListener>> listenerMap = new HashMap<>();

    @Override
    public void connect() throws MqttException {
        String broker = "tcp://test.mosquitto.org:1883";
        String clientId = "JavaSample";

        MemoryPersistence persistence = new MemoryPersistence();

        try {
            this.client = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions opts = new MqttConnectOptions();
            opts.setCleanSession(true);

            logger.info("Connecting to broker: " + broker);
            this.client.connect(opts);
            logger.info("Connected");

        } catch (MqttException me) {
            logger.error("reason " + me.getReasonCode());
            logger.error("msg " + me.getMessage());
            logger.error("loc " + me.getLocalizedMessage());
            logger.error("cause " + me.getCause());
            logger.error("excep " + me);
            me.printStackTrace();
        }
    }

    @Override
    public void subscribe(String topic, MqttListener listener) throws Exception {
        this.client.subscribe(topic, (mqttTopic, mqttMessage) -> {
            logger.info(String.format("Received message: %s - %s", mqttTopic, new String(mqttMessage.getPayload(), StandardCharsets.UTF_8)));

            listener.onMessageReceived(
                new com.bensivo.mqttadapter.mqtt.MqttMessage(mqttTopic, mqttMessage.getPayload())
            );
        });
    }
}
