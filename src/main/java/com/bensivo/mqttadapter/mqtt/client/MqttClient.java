package com.bensivo.mqttadapter.mqtt.client;

import com.bensivo.mqttadapter.mqtt.listener.MqttListener;

/**
 * Interface for all the MQTT functions we use in this application.
 * Exists to separate our choice of MQTT client library from the rest of the application
 */
public interface MqttClient {

    public void connect() throws Exception;

    /**
     * Add a listener, to be called whenever any message on the given topic is received.
     *
     * NOTE: Open question whether a call to this function should start a subscription with that topic.
     * 
     * @param listener
     */
    public void subscribe(String topic, MqttListener listener) throws Exception;
}
