package com.bensivo.mqttadapter.mqtt;

public record MqttMessage(String topic, byte[] payload) { }
