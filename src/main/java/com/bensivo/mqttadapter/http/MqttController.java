package com.bensivo.mqttadapter.http;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import com.bensivo.mqttadapter.mqtt.listener.MqttListenerProvider;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class MqttController {

    @Autowired
    private MqttListenerProvider mqttListenerProvider;

    Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping(path = "/subscribe/**")
    public Flux<ServerSentEvent<String>> streamEvents(ServerWebExchange exchange) {
        try {
            var request = exchange.getRequest();
            var topic = request.getPath().toString().replace("/subscribe/", "");

            logger.info(String.format("Request for topic: %s\n", topic));
            var listener = mqttListenerProvider.getListener(topic);
            if (listener == null) {
                throw new Exception("No listener registered for topic");
            }

            var flux = listener.getFlux()
                    .map(mqttMessage -> {
                        return ServerSentEvent.<String>builder()
                                .event(topic)
                                .data(new String(mqttMessage.payload(), StandardCharsets.UTF_8))
                                .build();
                    })
                    .take(Duration.ofSeconds(10))
                    .startWith(Collections.singleton(
                            ServerSentEvent.<String>builder()
                                    .id("start")
                                    .event(topic)
                                    .data("Starting stream - listening for 10 seconds")
                                    .build()));

            return Flux.concat(flux, Flux.just(ServerSentEvent.<String>builder()
                    .id("end")
                    .event(topic)
                    .data("Ending stream")
                    .build()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        var event = ServerSentEvent.<String>builder()
                .id("0")
                .event("bensivo/events")
                .data("An error ocurred")
                .build();
        return Flux.from(Mono.just(event));
    }
}
