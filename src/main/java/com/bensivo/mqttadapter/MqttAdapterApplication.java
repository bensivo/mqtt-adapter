package com.bensivo.mqttadapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.bensivo.mqttadapter.mqtt.client.MqttClient;
import com.bensivo.mqttadapter.mqtt.listener.MqttListener;
import com.bensivo.mqttadapter.mqtt.listener.MqttListenerProvider;


@SpringBootApplication
public class MqttAdapterApplication {

	@Autowired
    private MqttClient mqttAdapterClient;

	@Autowired
    private MqttListenerProvider mqttListenerProvider;

	Logger logger = LoggerFactory.getLogger(MqttAdapterApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MqttAdapterApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
    public void bootstrapApplication() throws Exception{
        this.mqttAdapterClient.connect();

		var topics = new String[] {
			"bensivo/events"
		};

		// Subscribe to all topics, creating a listener for each one
		for(String topic: topics) {
			var listener = new MqttListener();

			logger.info("Subscribing to topic: " + topic);
			this.mqttAdapterClient.subscribe(topic, listener);
			this.mqttListenerProvider.setListener(topic, listener);
		}
    } 
}
