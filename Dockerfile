FROM gradle:8.1.1-jdk17 as build
WORKDIR /home/gradle 

COPY . .
RUN gradle build --no-daemon

FROM amazoncorretto:17-alpine3.14

WORKDIR /app
COPY --from=build /home/gradle/build/libs/mqtt-adapter-0.0.1-SNAPSHOT.jar /app
CMD [ "java", "-jar", "/app/mqtt-adapter-0.0.1-SNAPSHOT.jar" ]